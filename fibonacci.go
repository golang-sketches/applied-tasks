package main

import (
	"fmt"
)

var fibSlice = []int{0, 1, 1}

func main() {
	fmt.Println(fibonacci(15))
}

func fibonacci(n int) (res int) {

	if len(fibSlice) > n && fibSlice[n] >= 0 {
		res = fibSlice[n]
		return res
	}

	res = fibonacci(n-1) + fibonacci(n-2)
	fibSlice = append(fibSlice, res)
	return res
}
