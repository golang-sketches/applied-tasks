package main

import "fmt"

const LIM = 45

var facts [LIM]uint64

func main() {
	var result uint64 = 0
	for i := uint64(0); i < LIM; i++ {
		result = FactorialMemoization(uint64(i))
		fmt.Printf("The factorial value for %d is %d\n", uint64(i), uint64(result))
	}
}

func FactorialMemoization(n uint64) (res uint64) {
	if facts[n] != 0 {
		res = facts[n]
		return res
	}

	if n > 0 {
		res = n * FactorialMemoization(n-1)
		return res
	}

	return 1
}